var highlightCount;
var wordCount;
var alreadyClicked = false
jQuery(document).ready(function () {
    jQuery('#calcButton').click(function () {
        if (alreadyClicked == false) {
            jQuery('#percentage').delay(500).fadeIn()
            jQuery('.percentageExplain').delay(500).fadeIn()
            jQuery('.clickButtonExplain').fadeOut()
            jQuery('#samsonAnimation').attr('src', 'images/tappingFoot.gif');
            setTimeout(
                function () {
                    jQuery('#samsonAnimation').attr('src', 'images/tappingFoot.gif');
                }, 700);
            chrome.tabs.query({
                active: true,
                currentWindow: true
            }, function (tabs) {
                chrome.tabs.sendMessage(tabs[0].id, {
                    highlight: "highlight"
                })
            });
            alreadyClicked = true
        }
    })
})

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        if (request.highlightCount) {
            highlightCount = request.highlightCount
            alert(highlightCount)
        }
        if (request.wordCount) {
            jQuery('#samsonAnimation').attr('src', 'images/standing.gif');
            wordCount = request.wordCount
            alert("Word Count = " + wordCount)
            var percentage = Math.round((highlightCount / wordCount) * 100)
            i = 0
            l = 1
            loop();

            function loop() {
                setTimeout(function () {
                    jQuery('#percentageCircle').removeClass("p" + i)
                    jQuery('#percentageCircle').addClass("p" + l)
                    jQuery('#percentage').text(l + "%")
                    i++;
                    l++;
                    if (i < percentage) {
                        loop()
                    }
                }, 20)
            }


        }


    });
